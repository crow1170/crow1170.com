<html>
  <head>
    <link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.min.css" rel="stylesheet">
    <script async src="//ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
    <script async src="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/js/bootstrap.min.js"></script>
  </head>
  <body>
    <div class="container">
      <div class="row well">
        <h1>Notes</h1>
        <div class="span6">
          <p>This page is a collection of errant thoughts organized by time. It's meant to be reviewed and shared.</p>
        </div><!--/span6-->
        <div class="span4">
          <a href="#" class="btn btn-primary disabled">Add Note</a>
          <a href="#" class="btn btn-primary disabled">Edit Note</a>
          <a href="#" class="btn btn-primary disabled">Share Note</a>
        </div><!--/span4-->
      </div><!--/row well-->
      <div class="well">
        <a name="386570" ></a>
        <h2>386570</h2>
        <em>2200 5 Feb EST</em>
        <p>Made Notes page. Each note is referred to by hours since the epoch; unix timestamp/3600. I plan to have notes be managed by database and use tags. Notes <em>must</em> be available to create from phone.</p>
        <!--p>Tags: <span class="label">span</span></p-->
      </div><!--/well-->
    </div><!--/container-->
  </body>
</html>
