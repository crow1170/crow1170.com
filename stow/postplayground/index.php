<html>
  <head>
    <link rel="icon" type="image/png" href="http://ocelot.aul.fiu.edu/~cknow003/assets/favicon.ico">
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css" rel="stylesheet">
    <link href="//netdna.bootstrapcdn.com/bootswatch/3.0.3/cerulean/bootstrap.min.css" rel="stylesheet">
    <style>
      /* Sticky footer styles
      -------------------------------------------------- */
      html,
      body {
        height: 100%;
        /* The html and body elements cannot have any padding or margin. */
      }

      /* Wrapper for page content to push down footer */
      #wrap {
        min-height: 100%;
        height: auto;
        /* Negative indent footer by its height */
        margin: 0 auto -60px;
        /* Pad bottom by footer height */
        padding: 0 0 60px;
      }

      /* Set the fixed height of the footer here */
      #footer {
        height: 60px;
        background-color: #f5f5f5;
      }

      #wrap > .container {
        padding: 60px 15px 0;
      }
    </style>
    <title>Chris Knowles CKnow003@fiu.edu</title>
  </head>
  <body>
    <div class="wrap">
            <!-- Fixed navbar -->
      <div class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Crow1170</a>
          </div>
          <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
              <li class="active"><a href="http://crow1170.com">Home</a></li>
              <li><a href="#">PostPlayground</a></li>
            </ul>
          </div><!--/.nav-collapse -->
        </div>
      </div>
      <div class="container">
        <div class="jumbotron">
          <h1>PostPlayground</h1>
          <p class="lead">Post here to see your post below.</p>
        </div><!--/jumbotron-->
        <div class="row">
          <div class="col-md-8">
            <a name="about"></a>
            <h2>Recent Posts</h2>
            <div class="well">
              <ul>
              <?php
                chdir(getcwd().'/posts');
                $ls = array_diff(scandir(getcwd()), Array('.', '..'));
                foreach($ls as $file){
                  $fh = fopen($file, 'r');
                  //echo('<li>'.fread($fh, filesize($file)).'</li>');
                  echo('<li>'.$file.'<br />'.fread($fh, filesize($file)).'</li>');
                  fclose($fh);
                }
              ?>
              </ul
            </div><!--/well-->
          </div><!--/col8-->
        </div><!--/row-->
      </div><!--/container-->
    </div><!--/wrap-->
    <div id="footer">
      <div class="container">
        <div class="row">
          <span class="pull-right"><span class="text-muted">Made with care by:</span> <a href="http://crow1170.com">Christopher Knowles</a></span>
        </div><!--/row-->
        <div class="row">
          <span class="pull-right"><a href="mailto:cknow003@fiu.edu">cknow003@fiu.edu</a></span>
        </div><!--/row-->
      </div><!--/container-->
    </div><!--/footer-->
    <!-- JS -->
    <script src="//code.jquery.com/jquery-2.1.0.min.js"></script>
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
  </body>
</html>
