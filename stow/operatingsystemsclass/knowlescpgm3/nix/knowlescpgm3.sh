#!/bin/bash
touch results.txt
echo "Knowles, Christopher" >> results.txt
echo "OS CGS3767" >> results.txt
echo "Linux script results" >> results.txt
echo "Login at: `date`" >> results.txt
x=
while [[ $x -ne "999" || $y -ne "999" ]]
do
  if [[ !$x ]]; then
    echo -n "Enter the first number and press [ENTER]: "
    read x
  fi
  echo -n "Enter the second number and press [ENTER]: "
  read y
  if [ $y -eq "0" ]; then
    echo "ERROR! The second number must not be zero."
  elif [ $y -ne "0" ]; then
    echo "\$x: $x"
    echo "\$y: $y"
    echo -n "\$x/\$y is "
    expr $x / $y
    x=
  fi
done
echo -n "Press [ENTER] to continue to the 'for loop' section."
read
for i in {1..100}
do
  echo -n "$i % 5 = "
  a=`expr $i % 5`
  echo $a
  echo $a >> results.txt
  b=$(($b + $a))
done
avg=$(($b / 100))
echo "Average of all mod expressions: $avg"
echo $avg >> results.txt
