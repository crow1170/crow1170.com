<?php
  if(isset($_GET['swatch'])){
    $swatch=$_GET['swatch'];
  }else{
    $swatch="yeti";
  }
?>
<html>
<head>
  <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css" rel="stylesheet">
  <link href="//netdna.bootstrapcdn.com/bootswatch/3.0.3/<?=$swatch;?>/bootstrap.min.css" rel="stylesheet">
  <style>
    /* Sticky footer styles
-------------------------------------------------- */

html,
body {
  height: 100%;
  /* The html and body elements cannot have any padding or margin. */
}

/* Wrapper for page content to push down footer */
#wrap {
  min-height: 100%;
  height: auto;
  /* Negative indent footer by its height */
  margin: 0 auto -60px;
  /* Pad bottom by footer height */
  padding: 0 0 60px;
}

/* Set the fixed height of the footer here */
#footer {
  height: 60px;
  background-color: #f5f5f5;
}
  </style>
  <script src="//netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
  <title>Chris Knowles cknow003@fiu.edu</title>
</head>
<body>
<div id="wrap">
  <div class="container">
    <h1>Help Me</h1>
    <hr>
    <div class="row">
      <div class="col-md-7">
        <?php
          if(!isset($_GET['class'])){$_GET['class']="UNKNOWN";}
          switch($_GET['class']){
            case "cts4348":
              ?><p><span class="lead">Bad News.</span> You're on your own, buddy. That class is hard.</p><?php
              break;
            case "cgs4854":
              ?><p><span class="lead">Looks like you want help with CGS4854.</span> Happy to help! Meet me after class or send me an email at <a href="mailto:cknow003@fiu.edu">cknow003@fiu.edu</a>.</p><p>There may also be tips up on my <a href="http://ocelot.aul.fiu.edu/~cknow003/">student page</a>.<?php
              break;
            case "cis4431":
              ?><p><span class="lead">Looks like you want help with CIS4431.</span> Happy to help! Meet me after class or send me an email at <a href="mailto:cknow003@fiu.edu">cknow003@fiu.edu</a>.</p><?php
              break;
            default:
              ?>
                <p><span class="lead">Uh-oh!</span> I don't recognize the class you requested. Sorry.</p>
              <?php
          }
        ?>
      </div><!--/col-md-7-->
      <div class="col-md-offset-1 col-md-4">
        <h2>Navigate Swatches</h2>
        <div class="well">
          <p>Change swatches, courtesy of <a href="http://www.bootstrapcdn.com/#bootswatch_tab">BootSwatch</a>:</p>
          <ul>
            <li><a href="/operatingsystemsclass/index.php?swatch=yeti">Yeti (default)</a></li>
            <li><a href="/operatingsystemsclass/index.php?swatch=amelia">Amelia</a></li>
            <li><a href="/operatingsystemsclass/index.php?swatch=cosmo">Cosmo</a></li>
            <li><a href="/operatingsystemsclass/index.php?swatch=cyborg">Cyborg</a></li>
            <li><a href="/operatingsystemsclass/index.php?swatch=cerulean">Cerulean</a></li>
            <li><a href="/operatingsystemsclass/index.php?swatch=slate">Slate</a></li>
          </ul>
        </div><!--/well-->
        <div class="well">
          <p class="lead">Need help in this class?</p>
          <p><a href="/operatingsystemsclass/helpme.php?class=cgs4854">Help me in CGS4854 ></a></p>
          <p><a href="/operatingsystemsclass/helpme.php?class=cts4348">Help me in CTS4348 ></a></p>
          <p><a href="/operatingsystemsclass/helpme.php?class=cis4431">Help me in CIS4431 ></a></p>
        </div><!--/well-->
      </div><!--/col-md-4-->
    </div><!--/row-->
  </div><!--/container-->
</div><!--/wrap-->
<div id="footer">
  <div class="container">
    <p>Change swatches, courtesy of <a href="http://www.bootstrapcdn.com/#bootswatch_tab">BootSwatch</a>:</p>
      <table>
        <tr>
          <td><a href="/operatingsystemsclass/index.php?swatch=yeti">Yeti (default)</a></td>
          <td><a href="/operatingsystemsclass/index.php?swatch=amelia">Amelia</a></td>
          <td><a href="/operatingsystemsclass/index.php?swatch=cosmo">Cosmo</a></td>
          <td><a href="/operatingsystemsclass/index.php?swatch=cyborg">Cyborg</a></td>
          <td><a href="/operatingsystemsclass/index.php?swatch=cerulean">Cerulean</a></td>
          <td><a href="/operatingsystemsclass/index.php?swatch=slate">Slate</a></td>
        </tr>
      </table>
  </div><!--/container-->
</div><!--/footer-->
</body>
</html>

