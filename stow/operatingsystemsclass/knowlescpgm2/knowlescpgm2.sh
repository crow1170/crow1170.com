#!/bin/bash
echo "Program Two Starting"
mkdir dna
wc NC_002516.gbk > ./dna/dnaCount.txt
cat ./dna/dnaCount.txt > ./dnaCount2.txt
cat ./dna/dnaCount.txt NC_002516.gbk dnaCount2.txt > ./dna/merged.txt
grep -o tataga NC_002516.gbk > tatagaAmts.txt
cat tatagaAmts.txt
grep -o tataga tatagaAmts.txt >> ./dna/merged.txt
tail -3 dna/merged.txt
head -3 dna/merged.txt
echo "Program Two Ended"
