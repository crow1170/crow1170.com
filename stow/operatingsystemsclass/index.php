<?php
  if(isset($_GET['swatch'])){
    $swatch=$_GET['swatch'];
  }else{
    $swatch="yeti";
  }
?>
<html>
<!-- It's really mine. I'm wearing a red hoodie and black glasses -->
<head>
  <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css" rel="stylesheet">
  <link href="//netdna.bootstrapcdn.com/bootswatch/3.0.3/<?=$swatch;?>/bootstrap.min.css" rel="stylesheet">
  <style>
    /* Sticky footer styles
-------------------------------------------------- */

html,
body {
  height: 100%;
  /* The html and body elements cannot have any padding or margin. */
}

/* Wrapper for page content to push down footer */
#wrap {
  min-height: 100%;
  height: auto;
  /* Negative indent footer by its height */
  margin: 0 auto -60px;
  /* Pad bottom by footer height */
  padding: 0 0 60px;
}

/* Set the fixed height of the footer here */
#footer {
  height: 60px;
  background-color: #f5f5f5;
}
  </style>
  <script src="//netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
  <title>Chris Knowles cknow003@fiu.edu</title>
</head>
<body>
<div id="wrap">
  <div class="container">
    <?php
      if(isset($_GET['fw']) && $_GET['fw']){?>
    <div class="ialert alert-success">
      <p>You were successfully redirected from jump. Make a note of this URL so you don't have to be forwarded later.</p>
    </div>
<?php      }

    ?>
    <h1>Website Construction and Management</h1>
    <hr>
    <div class="row">
      <div class="col-md-7">
        <h2>Assignment One</h2>
        <div class="well">
          <p>Requirements for assignment one can be found at mrobi002's <a href="http://users.cis.fiu.edu/~mrobi002/teaching/cgs4854/cgs4854pgm1c">website</a>.</p>
          <p>Let's tackle this together.</p>
          <h3>Use HTML, Tables, paragraphs, breaks, CSS, and PHP</h3>
            <p>At a minimum, we can accomplish three of these with this one line; a paragraph tag in HTML served by a PHP script. Simply naming the HTML a PHP one could satisfy this, but we'll do better.</p>
            <br/>
            <p>By adding extra space we've employed the break tag.</p>
            <p>By using BootStrap and BootSwatch the page becomes beatifully styled with CSS in just 2 lines. Visit <a href="http://www.bootstrapcdn.com/">BootStrapCDN</a> for more info.</p>
            <p>We'll revisit tables in a little bit</p>
          <h3>Use multiple colors</h3>
            <p>Luckily, CSS has handled this already. Hurray!</p>
          <h3>Do at least four pages, passing and displaying data between them</h3>
            <p>Let's make use of PHP, tables, and CSS for this one.</p>
            <p>We'll add a table of available swatches in the footer, complete with links that point to GET requests; http://crow1170.com/operatingsystemsclass/index.php?thisStuffAfterTheQMark.</p>
            <p>We can retrieve GET variables by accessing the $_GET array in PHP. We'll check if the $_GET'swatch'] variable exists first. If it doesn't, we'll set it to the default swatch to use.</p>
            <p>Then we can edit our CSS link to request the swatch in the $_GET['swatch'] variable. This will cange the look of the site with the click of a button. We'll make sure we have at least four different looking pages available. There are plenty to choose from.
          <h3>Add Graphics</h3>
            <p>Should be easy enough:</p>
            <img src="http://sd.keepcalm-o-matic.co.uk/i/thank-you-for-reading.png" alt="thanks for reading" / >
        </div>
      </div><!--/col-md-7-->
      <div class="col-md-offset-1 col-md-4">
        <h2>Navigate Swatches</h2>
        <div class="well">
          <p>Change swatches, courtesy of <a href="http://www.bootstrapcdn.com/#bootswatch_tab">BootSwatch</a>:</p>
          <ul>
            <li><a href="/operatingsystemsclass/index.php?swatch=yeti">Yeti (default)</a></li>
            <li><a href="/operatingsystemsclass/index.php?swatch=amelia">Amelia</a></li>
            <li><a href="/operatingsystemsclass/index.php?swatch=cosmo">Cosmo</a></li>
            <li><a href="/operatingsystemsclass/index.php?swatch=cyborg">Cyborg</a></li>
            <li><a href="/operatingsystemsclass/index.php?swatch=cerulean">Cerulean</a></li>
            <li><a href="/operatingsystemsclass/index.php?swatch=slate">Slate</a></li>
          </ul>
        </div><!--/well-->
        <div class="well">
          <p class="lead">Need help in this class?</p>
          <p><a href="/operatingsystemsclass/helpme.php?class=cgs4854">Help me in this class ></a></p>
          <p><a href="/operatingsystemsclass/helpme.php?class=cts4348">Help me in CTS4348 ></a></p>
          <p><a href="/operatingsystemsclass/helpme.php?class=cis4431">Help me in CIS4431 ></a></p>
        </div><!--/well-->
      </div><!--/col-md-4-->
    </div><!--/row-->
  </div><!--/container-->
</div><!--/wrap-->
<div id="footer">
  <div class="container">
    <p>Change swatches, courtesy of <a href="http://www.bootstrapcdn.com/#bootswatch_tab">BootSwatch</a>:</p>
      <table>
        <tr>
          <td><a href="/operatingsystemsclass/index.php?swatch=yeti">Yeti (default)</a></td>
          <td><a href="/operatingsystemsclass/index.php?swatch=amelia">Amelia</a></td>
          <td><a href="/operatingsystemsclass/index.php?swatch=cosmo">Cosmo</a></td>
          <td><a href="/operatingsystemsclass/index.php?swatch=cyborg">Cyborg</a></td>
          <td><a href="/operatingsystemsclass/index.php?swatch=cerulean">Cerulean</a></td>
          <td><a href="/operatingsystemsclass/index.php?swatch=slate">Slate</a></td>
        </tr>
      </table>
  </div><!--/container-->
</div><!--/footer-->
</body>
</html>

