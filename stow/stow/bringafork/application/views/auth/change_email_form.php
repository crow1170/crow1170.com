<?php
$password = array(
	'name'	=> 'password',
	'id'	=> 'password',
	'size'	=> 30,
);
$email = array(
	'name'	=> 'email',
	'id'	=> 'email',
	'value'	=> set_value('email'),
	'maxlength'	=> 80,
	'size'	=> 30,
);
?>
<html>
<head>
  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
  <script src="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.1/js/bootstrap.min.js"></script>
  <link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.1/css/bootstrap-combined.min.css" rel="stylesheet">
</head>
<body>
<div class="container">
  <div class="hero-unit">
    <h1>Change your account email</h1>
    <br>
    <?php echo form_open($this->uri->uri_string()); ?>
      <table>
        <tr>
          <td><?php echo form_label('Password: ', $password['id']); ?></td>
          <td><?php echo form_password($password); ?></td>
          <td style="color: red;"><?php echo form_error($password['name']); ?><?php echo isset($errors[$password['name']])?$errors[$password['name']]:''; ?></td>
        </tr>
        <tr>
          <td><?php echo form_label('New email address: ', $email['id']); ?></td>
          <td><?php echo form_input($email); ?></td>
          <td style="color: red;"><?php echo form_error($email['name']); ?><?php echo isset($errors[$email['name']])?$errors[$email['name']]:''; ?></td>
        </tr>
      </table>
      <br>
      <?php echo form_submit('change', 'Send confirmation email'); ?>
      <?php echo form_close(); ?>
    </div>
  </div>
</body>
</html>
