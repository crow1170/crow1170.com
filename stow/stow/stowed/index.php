<html>
<head>
	<link rel="stylesheet" href="http://code.jquery.com/mobile/1.3.0/jquery.mobile-1.3.0.min.css" />
	<script src="http://code.jquery.com/jquery-1.8.2.min.js"></script>
	<script src="http://code.jquery.com/mobile/1.3.0/jquery.mobile-1.3.0.min.js"></script>
</head>
<body>
	<div data-role="header" data-position="fixed" style="float:left; width:100%;">
	    <h1 style="text-align:left;">Crow1170 - Chris Knowles' Personal Website</h1>
	    <!--a href="#" data-icon="gear" class="ui-btn-right">Options</a-->
	    <!--div data-role="navbar">
	        <ul>
	            <li><a href="#">About Me</a></li>
	            <li><a href="#">Contact</a></li>
	            <li><a href="#">Projects</a></li>
	            <li><a href="#">MIA-Buzz</a></li>
	            <li><a href="#">Unblockr</a></li>
	        </ul>
	    </div--><!-- /navbar -->
	</div><!-- /header -->
	<ul data-role="listview" data-inset="true">
		<li><a data-ajax="false" href="/mia-buzz">MIA-Buzz</a></li>
		<li><a data-ajax="false" href="/unblockr/">Unblockr</a></li>
		<li><a data-ajax="false" href="http://myhonors.fiu.edu">myHonors</a></li>
		<li><a data-ajax="false" href="/projects/baf">#BringAFork</a></li>
		<li><a data-ajax="false" href="/chi">CHI</a></li>
	</ul>
</body>
</html>
