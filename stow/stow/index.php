<html ng-app>
<head>
  <link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.min.css" rel="stylesheet">

  <script async src="https://ajax.googleapis.com/ajax/libs/angularjs/1.0.7/angular.min.js"></script>
  <script async src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
  <script async src="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/js/bootstrap.min.js"></script>
  <script>
    function AppCtrl($scope){
      $scope.display = "done";
    }
  </script>
</head>
<body ng-controller="AppCtrl">
  <div class="container">
    <div class="well">
      <h2>Hi, I'm Christopher Knowles</h2>
   </div>
    <div class="well">
      <h2>Why not check out some of the stuff 
        <select ng-model="display" style="width:111px;">
          <option value="done">I've done</option>
          <option value="doing">I'm doing</option>
          <option value="want">I want to do</option>
        </select>
      ?</h2>
      <div ng-show="display == 'done'">
        <dl>
          <dt><a href="/mrcourierexpress">MRCourierExpress</a></dt>
            <dd>Some good friends of mine needed a website for their courier service and I was happy to help.</dd>
          <dt><a href="/mia-buzz">MIA-Buzz</a></dt>
            <dd>I attended a hackathon and had 48 hours to design something- anything- that used sponsor APIs. I made this and won a Sphereo robot and a copy of Halo 4.</dd>
          <dt><a href="/unblockr/">Unblockr</a></dt>
            <dd>During the aforementioned hackathon I experienced some burnout/writer's block. I decided to work with someone else who didn't have much experience and wanted to see how they could make a simple idea become reality.</dd>
          <dt><a href="http://myhonors.fiu.edu">myHonors</a></dt>
          <dt><a href="/projects/baf">#BringAFork</a></dt>
          <dt><a href="/chi">CHI</a></dt>
        </dl>
      </div>
      <div ng-show="display == 'doing'">
        <!--ul>
          <li><a href="http://myhonors.fiu.edu">myHonors</a></li>
          <li><a href="/projects/baf">#BringAFork</a></li>
        </ul-->
        <p>Oh, god, this is embaressing. Look, I promise I'll update this section when I have some time. I promise.</p>
      </div>
      <div ng-show="display == 'want'">
        <!--ul>
          <li><a href="/chi">CHI</a></li>
        </ul-->
        <p>Oh, god, this is embaressing. Look, I promise I'll update this section when I have some time. I promise.</p>
      </div>
    </div>
    <div class="well">
      <p>You can keep in touch with me through:</p>
      <address><small><ul class="inline">
        <li>Email: <a href="mailto:c.knowles.117@gmail.com">c.knowles.117@gmail.com</a></li>
        <li>Grooveshark: <a href="http://grooveshark.com/#!/cknowles117">cknowles117</a></li>
        <li>Twitter: <a href="https://twitter.com/CKnowles">CKnowles</a></li>
        <li>Reddit: <a href="http://www.reddit.com/user/crow1170/">Crow1170</a></li>
      </ul></small></address>
    </div>
  </div>
</body>
</html>
