<html>
  <head>
    <meta name=viewport content="width=device-width, initial-scale=1">
    <link async defer href="bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link async defer href="bower_components/bootstrap-material-design/dist/css/ripples.min.css" rel="stylesheet">
    <link async defer href="bower_components/bootstrap-material-design/dist/css/material-fullpalette.min.css" rel="stylesheet">
  </head>
  <body>
    <div class="container">
      <div class="bs-component"><!-- Navbar -->
        <div class="navbar navbar-default navbar-material-blue">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="javascript:void(0)">Crow1170</a>
          </div>
          <div class="navbar-collapse collapse navbar-responsive-collapse">
            <ul class="nav navbar-nav">
              <li><a href="/">Home</a></li>
              <li><a href="/play">Play</a></li>
              <li><a href="/books">Books</a></li>
              <li class="active"><a href="/fuel">Fuel</a></li>
            </ul>
          </div>
        </div>
      </div><!-- ./navbar -->
      <div class="well well-material-blue-900 col-md-12">
        <div class="row">
          <div class="well well-material-deep-orange col-md-4 col-md-offset-1">
            <dl class="dl-horizontal">
