<html ng-app>
<head>                                                                    <meta name=viewport content="width=device-width, initial-scale=1">
  <link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.min.css" rel="stylesheet">
  <script async src="https://ajax.googleapis.com/ajax/libs/angularjs/1.0.7/angular.min.js"></script>
  <script async src="//ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
  <script async src="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/js/bootstrap.min.js"></script>
  <script>
    function AppCtrl($scope){
      $scope.display = "doing";
    }
  </script>
</head>
<body ng-controller="AppCtrl">
  <div class="container">
    <div class="row well">
      <div class="span8">
        <h2>Hi, I'm Chris Knowles</h2>
      </div>
      <div class="span3">
        <a style="margin-bottom:4px;" class="btn btn-primary active" href="/">Home</a>
        <a style="margin-bottom:4px;" class="btn btn-primary" href="/play">Play</a>
        <a style="margin-bottom:4px;" class="btn btn-primary" href="/fuel">Fuel</a>
      </div>
    </div>
    <div class="row well">
      <h2>Why not check out some of the stuff 
        <select ng-model="display" style="width:111px;">
          <option value="doing">I'm doing</option>
          <option value="done">I've done</option>
          <option value="want">I want to do</option>
        </select>
      ?</h2>
      <div ng-show="display == 'done'">
        <dl>
          <dt><a href="/mia-buzz">MIA-Buzz</a></dt>
            <dd>I attended a hackathon and had 48 hours to design something- anything- that used sponsor APIs. I made this and won a Sphereo robot and a copy of Halo 4.</dd>
          <dt>Unblockr</dt>
            <dd>During the aforementioned hackathon I experienced some burnout/writer's block. I decided to work with someone else who didn't have much experience and wanted to see how they could make a simple idea become reality.</dd>
          <dt><a href="http://myhonors.fiu.edu">myHonors</a></dt>
            <dd>An ongoing project for the FIU Honors College provides an online platform for as much of the College as we can digitize. Attendance records, Applications, forums and more.</dd>
          <dt>#BringAFork</dt>
            <dd>In an attempt to meet new friends and create a sense of community in the dorm, I started making dinner for everyone Monday nights. The meals always line up with the movie we watch are absolutely free of charge, all you have to do is #BringAFork.</dd>
          <dt><a href="/chi">CHI</a></dt>
            <dd>Some practice in AngiularJS used as a class project. The project was designed to show how challenging it can be to ask several questions on one page. Using CSS tabs and Angular data binding I made one page that felt like many, eliminating the need for POSTs and whatnot.</dd>
        </dl>
      </div>
      <div ng-show="display == 'doing'">
        <dl>
          <dt><a href="//crow1170.com/pumpkin">FIU Treasury Web Site</a></dt><dd>For a long time, the treasury's web page has been like those of its peers: Straight out of the 90's. Not much longer, though. After signing on as an intern at the treasury my first assignment is to wrap up their new site, complete with cool new data visualization techniques and procedures that make updating the financial info almost automatic.</dd>
          <dt><a href="//crow1170.com/saffron">VoteAid</a></dt><dd>Working with some good folks dedicated to applying the sensibilities of dating sites to politics. We're making a web app that will use responses to political questions to help you find which candidate you like the most.</dd>
          <dt><a href="https://bitbucket.org/crow1170/codeigniter-variant/">CodeIgniter Variant</a></dt>
            <dd>I find most of my web projects start out the same; making the same minor changes to the stock CodeIgniter framework. This repo is a collection of those changes stacked on top of CodeIgniter 2.1-stable.<br>Notable features include Bootswatch templates, tank_auth user accounts, and preconfigured development files.</dd>
          <dt><a href="http://campus.codeschool.com/courses/discover-drive/intro">Discover Drive</a></dt>
            <dd>I'm learning to use the Drive API with hopes of creating a project managment web app. Know any other good learning material for it? Drop me a line.</dd>
        </dl>
      </div>
      <div ng-show="display == 'want'">
        <dl>
          <dt><a href="http://tedxfiu.com/">TEDxFIU</a></dt><dd>I have an opportunity to have my own TED Talk! My university has a TEDx event scheduled and are accepting applications to speak. I want to make the case for revolutionizing the US public library system, making them the center for open source projects by providing commonly used tools like web hosting, power drills, and 3D printers.</dd>
          <dt>FPV</dt><dd>I got myself an RC plane and flying it has been pretty nice, but what really interests me is flying remotely and autonomously. I can't think of anything more exciting than commanding a fleet of UAVs, zipping and zooming in sync with each other.</dd>
          <dt>Farm</dt><dd>Ever since I first made a list of goals for my life, growing food has been on it. There's something deeply satisfying about the prospect of being responsible for something from seed to stomach. I don't think that, as a species, we've found all or even the best kinds of societies to live in and I'd like to try living in one where each citizen is involved with things in proportion to how important that thing is to them.</dd>
          <!--dt><a href="/#"></a></dt><dd></dd-->
        </dl>
      </div>
    </div>
    <div class="row well">
      <p>You can keep in touch with me through:</p>
      <address><small><ul class="inline">
        <li>Email: <a href="mailto:cknowles117@gmail.com">cknowles117@gmail.com</a></li>
        <li>GitHub: <a href="https://github.com/crow1170">Crow1170</a></li>
        <li>Twitter: <a href="https://twitter.com/CKnowles">CKnowles</a></li>
        <li>Reddit: <a href="http://www.reddit.com/user/crow1170/">Crow1170</a></li>
      </ul></small></address>
    </div>
  </div>
</body>
</html>
