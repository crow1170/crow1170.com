==================================================================
https://keybase.io/crow1170
--------------------------------------------------------------------

I hereby claim:

  * I am an admin of http://crow1170.com
  * I am crow1170 (https://keybase.io/crow1170) on keybase.
  * I have a public key with fingerprint 7B5E 9DC5 B03B 343B 72B3  6442 6B3E 740E 5A01 B6F6

To claim this, I am signing this object:

{
    "body": {
        "client": {
            "name": "keybase.io node.js client",
            "version": "0.4.16"
        },
        "key": {
            "fingerprint": "7b5e9dc5b03b343b72b364426b3e740e5a01b6f6",
            "host": "keybase.io",
            "key_id": "6B3E740E5A01B6F6",
            "uid": "8a0c9c2323cbdb144caa3bc9fa595000",
            "username": "crow1170"
        },
        "merkle_root": {
            "ctime": 1408291375,
            "hash": "1269e548532900f869958303103ad910067b5ed4031ac0720c2dcedad1010067aaf7cf0df3a8d70dbb1417bf324454935451c76b7982af7cb743698a77fd444f",
            "seqno": 40097
        },
        "service": {
            "hostname": "crow1170.com",
            "protocol": "http:"
        },
        "type": "web_service_binding",
        "version": 1
    },
    "ctime": 1408291688,
    "expire_in": 157680000,
    "prev": "5ee74d4e1df514f5cab367a2e867d21b5c37e3ccebfe6b864b01d89efbbd4a42",
    "seqno": 4,
    "tag": "signature"
}

with the aforementioned key, yielding the PGP signature:

-----BEGIN PGP MESSAGE-----
Version: GnuPG v1

owFdkn9QFGUYx+84EDFUGs0xJTgXdcDgenf33V8nglLQ5M+Bgoaf176773LLj9vr
7kSBsBgbkZwmnWpII3QiarwCJkOsDtIwshRR4poGKXPUUkZSB2csBLVdhma0/3bf
5/N83+/3ed7dsy2mEHNpim9OnrnztvnUCDI9f3PQWU0gTa4k7NWEVKZil8/4conl
mLATpbgSiV5sUzWrS5OxrcRrnWYSiQrs8aqaS6eADdpIlqhJNHijW1Fdxdjj9qiG
GMEhBguyxCBAIxrSiKMQzUJIsYjGHASYEQGJWIXVNZ2a1/fQtcSUpkOV9VM2jU7X
+XRmDSDT2AyD3zxV4EUgCRJFU7SEZERCKIkijSRBERmBAQAYoBd7pjNJHm0LSXLA
8FuOPaVl2OHRtKnUkk81EBICnhJImmN0R6LXqTeRFCtgBvIMTQkAKDwrCAxPA5oE
tCgLJACskVKG+okoAY4CEiVLWBZlEkwVRVHhJAXICi3yMgdkpNskOaTQFIQMFGgG
MqTEsYgTeMpAEQdpVuBFjlNkCKGiR/Dil10aYYcACFyN8eupUCVs2Dam9r9wNkkr
13vcHs2nSVqZXnH6fG67kdlX6TbILRg5WqdFHEh1yfrOHtwqqaMPz4Pl+UQCb3Wr
HuxQDYLhWF4fLzDuwRW6JoP1hcoQk7LCkFBhJFHfNCdSmGc5mSIRI9EcpiUJIwWz
iGchAqTMC1hBSIYipB5IqfsUi3VJr1rsEn2bPZioqQ+JDjWZQ0wzwkKMZ2uaFRH1
31vOLIwyfed8YnJkof+fC8Mp/vbT635rjF7WNth5YNKz4eLi2n3LXfYRa7bddfbu
qck99L2qRcOWSEBUK4PyKjBRqNVXvpWQEDt/WUzdkvruP9YF7+Ol/ssz9scf7cv6
KOJc75JFFiF4q3jPsa5Zk89F9fdZd7VZu9r2JryxY6JqH1XzRe1L971hmXG5qYcu
hnaPfTgePif2/CNrAprz0oKhsZvUmXSyN2PiY//dyNfGk0v7/N+OPvXj8a/q7mzv
yLpR+vbjJ1KSezYOuvdaEVydurjunV9ilzfXF8Yxw4HGDStNi49fm3vDnNZT1GL5
PKMkNHXHnZjtW0vSc1aqbs0R5+wZKwiOrlcGGv68unbY31F0nuk8VLDqSsnZgSUt
YSAQ07/j+3drf63arR55Nb6lybyt4s272XPrR8y7nxnIe9rRXLepfPyxT3reWxsa
3h57OTkP2izzigI/PWqaOWZt6Iq8MDRUkFS0k21JsGxvaGyIXJqfOxZM7Nl4bMaK
bvLIZ01ROeq9o3Paols7Olc0j/+dc+lC/rnIELH3Grr8KftNcdKLeUkFQaEpztoZ
6rw+2l/ZevHMrqv77cyJry/NXngwngwyP4jtua8HktYfq5x/u+H9D07+/mwgYl5v
6fXDT76wemeWvf/Az2FwW2ZFSSD7lfhbfttfwwssX1Kn44WVJw9ukk/k1+bPPDpi
uTJ0OPxf
=q1vN
-----END PGP MESSAGE-----

And finally, I am proving ownership of this host by posting or
appending to this document.

View my publicly-auditable identity here: https://keybase.io/crow1170

==================================================================
