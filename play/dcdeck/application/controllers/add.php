<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Add extends CI_Controller {
  public function index(){
    $this->card();
  }
  public function card(){
    $data['head']['customHead'] = '<script src="http://twitter.github.io/typeahead.js/releases/latest/typeahead.jquery.js"></script>';
    $this->load->view('head', $data['head']);
    $this->load->view('welcome_message');
    $this->load->view('foot');
  }
}
