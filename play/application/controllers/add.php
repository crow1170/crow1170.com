<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Add extends CI_Controller {
  public function index(){
    $dareAdded = false;
    if($_POST){
      if(isset($_POST['dare']) && isset($_POST['level'])){
        // Dare is well formed
        $this->load->library('input');
        $dare['dare'] = $_POST['dare'];
        $dare['level'] = $_POST['level'];
        $dare['ip'] = $this->input->ip_address();
        $this->db->insert('dares', $dare); 
        $this->load->view('addSuccess');
      }else{
        // Dare is malformed
        $this->load->view('addError');
      }
      $dareAdded = true;
    }else{
      $this->load->view('addDare');
    }
  }
}
