<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Jenga extends CI_Controller {
  public function index(){
    $this->random();
  }
  public function random($level = 0){
    if($level > 0){
      $this->db->where('level', $level); 
    }
    $query = $this->db->get('dares');
    $allDares = $query->result_array();
    if(count($allDares) != 0){
      $data['thisDare'] = $allDares[mt_rand(0, count($allDares) - 1)];
    }else{
      $data['thisDare']['dare'] = 'ERROR! ERROR! NO PUEDO ENCONTRAR MI CUERPO';
      $data['thisDare']['level'] = '0';
    }
    $this->load->view('jenga/showDare', $data);
  }
  public function add(){
    $data['dare'] = false;
    if($_POST){
      if(isset($_POST['dare'])){
        // Dare is well formed
        $this->load->library('input');
        $dare['dare'] = $_POST['dare'];
        $dare['level'] = $_POST['level'];
        $dare['ip'] = $this->input->ip_address();
        $this->db->insert('dares', $dare);
        $data['dare'] = $dare;
      }else{
        // Dare is malformed
        $this->load->view('jenga/addError');
      }
    }
    $easyQ = $this->db->query('SELECT COUNT(DISTINCT id) FROM `dares` WHERE `level` = 1');
    $mediumQ = $this->db->query('SELECT COUNT(DISTINCT id) FROM `dares` WHERE `level` = 2');
    $hardQ = $this->db->query('SELECT COUNT(DISTINCT id) FROM `dares` WHERE `level` = 3');
    $diamondQ = $this->db->query('SELECT COUNT(DISTINCT id) FROM `dares` WHERE `level` = 4');

    $easyR = $easyQ->row_array();
    $mediumR = $mediumQ->row_array();
    $hardR = $hardQ->row_array();
    $diamondR = $diamondQ->row_array();

    $data['easyCount'] = $easyR['COUNT(DISTINCT id)'];
    $data['mediumCount'] = $mediumR['COUNT(DISTINCT id)'];
    $data['hardCount'] = $hardR['COUNT(DISTINCT id)'];
    $data['diamondCount'] = $diamondR['COUNT(DISTINCT id)'];
    $this->load->view('jenga/addDare', $data);
  }
}
