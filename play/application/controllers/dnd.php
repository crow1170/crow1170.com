<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Dnd extends CI_Controller {
  public function __construct(){
    parent::__construct();
    $this->prefix = 'dnd/';
  }
  public function index(){
    $this->load->view($this->prefix.'welcome');
  }
  public function showChar($id = 1){
    $this->load->view($this->prefix.'char');
  }
  public function addChar(){
    $this->load->view($this->prefix.'welcome');
  }
}
