<html ng-app>
<head>
  <link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.min.css" rel="stylesheet">
  <script async src="https://ajax.googleapis.com/ajax/libs/angularjs/1.0.7/angular.min.js"></script>
  <script async src="//ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
  <script async src="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/js/bootstrap.min.js"></script>
  <script>
    function AppCtrl($scope){
      $scope.display = "done";
    }
  </script>
</head>
<body ng-controller="AppCtrl">
  <div class="container">
    <div class="row well">
      <div class="span8">
        <h2>The Game Room</h2>
      </div>
      <div class="span3">
        <a style="margin-bottom:4px;" class="btn btn-primary" href="/">Home</a>
        <a style="margin-bottom:4px;" class="btn btn-primary active" href="/play">Play</a>
        <a style="margin-bottom:4px;" class="btn btn-primary" href="/books">Books</a>
      </div>
    </div>
    <div class="row well">
      <h2>Here's what we can play:</h2>
      <dl>
        <dt><a href="<?=site_url().'dnd';?>">Dungeons &amp; Dragons</a></dt><dd>A custom suite of tools to assist in D&amp;D play.</dd>
        <dt><a href="<?=site_url().'jenga/random';?>">Jenga</a></dt><dd>Dare Jenga, best used in combination with a giant jenga tower.</dd>
        <dt><a class="disabled" href="<?=site_url().'dcdeck';?>">DC Deck Builder</a></dt><dd>(WIP) An attempt to be able to play the DCDBG over long distances.</dd>
      </dl>
    </div>
    <div class="row well">
      <p>You can keep in touch with me through:</p>
      <address><small><ul class="inline">
        <li>Email: <a href="mailto:c.knowles.117@gmail.com">c.knowles.117@gmail.com</a></li>
        <li>GitHub: <a href="https://github.com/crow1170">Crow1170</a></li>
        <li>Twitter: <a href="https://twitter.com/CKnowles">CKnowles</a></li>
        <li>Reddit: <a href="http://www.reddit.com/user/crow1170/">Crow1170</a></li>
        <li>Grooveshark: <a href="http://grooveshark.com/#!/cknowles117">cknowles117</a></li>
      </ul></small></address>
    </div>
  </div>
</body>
</html>

