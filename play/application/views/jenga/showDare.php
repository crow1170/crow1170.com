<html>
  <head>
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css" rel="stylesheet"> 
    <link href="//maxcdn.bootstrapcdn.com/bootswatch/3.3.2/journal/bootstrap.min.css" rel="stylesheet">
    <script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1">
  </head>
  <body>
    <!-- Begin page content -->
    <div class="container">
      <div class="well">
        <?php switch($thisDare['level']){
          case 4:
            echo('<h1><span class="label label-primary" >Diamond Dare</span></h1>');
                break;
                case 3:
            echo('<h1><span class="label label-danger" >Hard Dare</span></h1>');
                break;
                case 2:
            echo('<h1><span class="label label-warning" >Medium Dare</span></h1>');
                break;
                case 1:
            echo('<h1><span class="label label-success" >Easy Dare</span></h1>');
                break;
                default:
            echo('<h1><span class="label label-info" >Error</span></h1>');
                break;
              }?>
        <h2><?=$thisDare['dare'];?></h2>
      </div>
      <div class="well">
        <ul class="list-styled">
          <li style="display:inline;"><a style="width:6em;" class="btn btn-lg btn-success" href="<?=site_url().'jenga/random/1';?>">Easy</a></li>
          <li style="display:inline;"><a style="width:6em;" class="btn btn-lg btn-warning" href="<?=site_url().'jenga/random/2';?>">Medium</a></li>
        </ul>
        <ul class="list-styled">
          <li style="display:inline;"><a style="width:6em;" class="btn btn-lg btn-danger" href="<?=site_url().'jenga/random/3';?>">Hard</a></li>
          <li style="display:inline;"><a style="width:6em;" class="btn btn-lg btn-primary" href="<?=site_url().'jenga/random/4';?>">Diamond</a></li>
        </ul>
      </div>
      <div class="well">
        <p>Want to <a href="<?=site_url().'jenga/add';?>">add a dare</a>?</p>
      </div>
    </div>
  </body>
</html>

