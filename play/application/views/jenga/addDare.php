<html>
  <head>
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css" rel="stylesheet"> 
    <link href="//maxcdn.bootstrapcdn.com/bootswatch/3.3.2/journal/bootstrap.min.css" rel="stylesheet">
    <script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1">
  </head>
  <body>
    <div class="container">
      <div class="well">
        <h1>Add a Dare</h1>
        <?php if($dare){ ?>
          <p>Success! You added:</p>
          <p>"<?=$dare['dare'];?>"</p>
        <?php } ?>
        <form action="<?=current_url();?>" method="post">
          <input type="text" name="dare" id="dare"/>
          <select name="level">
            <option value='1'>Easy</option>
            <option value='2'>Medium</option>
            <option value='3'>Hard</option>
            <option value='4'>Diamond</option>
          </select>
          <input type="submit" value="Add Dare" />
        </form>
        <p>Existing dare counts:</p>
        <ul>
          <li><span class="label label-success">Easy:</span> <?=$easyCount;?></li>
          <li><span class="label label-warning">Medium:</span> <?=$mediumCount;?></li>
          <li><span class="label label-danger">Hard:</span> <?=$hardCount;?></li>
          <li><span class="label label-primary">Diamond:</span> <?=$diamondCount;?></li>
        </ul>
      </div>
      <div class="well">
        <p>Or, do some dares yourself:</p>
        <ul class="list-styled">
          <li style="display:inline;"><a style="width:6em;" class="btn btn-lg btn-success" href="http://www.crow1170.com/play/jenga/random/1">Easy</a></li>
          <li style="display:inline;"><a style="width:6em;" class="btn btn-lg btn-warning" href="http://www.crow1170.com/play/jenga/random/2">Medium</a></li>
        </ul>
        <ul class="list-styled">
          <li style="display:inline;"><a style="width:6em;" class="btn btn-lg btn-danger" href="http://www.crow1170.com/play/jenga/random/3">Hard</a></li>
          <li style="display:inline;"><a style="width:6em;" class="btn btn-lg btn-primary" href="http://www.crow1170.com/play/jenga/random/4">Diamond</a></li>
        </ul>
      </div>
    </div>
  </body>
</html>

