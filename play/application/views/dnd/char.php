<html>
  <head>
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
  </head>
  <body>
    <div class="container">
      <h1>Ander Brightwood</h1>
      <h2>Skills</h2>
      <p class="bg-info">Proficiency</p>
      <p class="bg-success">Expertise</p>
      <p class="bg-warning">Other bonuses</p>
      <table class="table table-condensed">
        <thead>
          <tr><th>Ability</th> 
                                       <th class="active">Total</th>
                                       <th>Base</th>
                                       <th>Mod sum</th>
                                       <th>Modifier(s)</th></tr></thead>
          <tr class="active"><td>Strength</td>    
                                       <td class="active">+1 (12)</td>
                                       <td>11</td>
                                       <td>+1</td>
                                       <td>Human ability increase (+1)</td></tr>
          <tr><td>Str Saving Throw</td><td class="active">+1</td>
                                       <td>+1</td>  
                                       <td>+0</td>
                                       <td></td></tr>
          <tr><td>Athletics</td>       <td class="info">+3</td>   
                                       <td>+1</td>  
                                       <td>+2</td>    
                                       <td>Proficiency (+2)</td></tr>
          <tr class="active"><td>Dexterity</td>    
                                       <td class="active">+3 (16)</td>
                                       <td>15</td>
                                       <td>+1</td>
                                       <td>Human ability increase (+1)</td></tr>
          <tr><td>Dex Saving Throw</td><td class="info">+5</td>
                                       <td>+3</td>  
                                       <td>+2</td>
                                       <td>Proficiency (+2)</td></tr>
          <tr><td>Acrobatics</td>      <td class="info">+5</td>   
                                       <td>+3</td>  
                                       <td>+2</td>    
                                       <td>Proficiency (+2)</td></tr>
          <tr><td>Sleight of Hand</td> <td class="warning">+4</td>   
                                       <td>+3</td>  
                                       <td>+1</td>    
                                       <td>Jack of All Trades (+1)</td></tr>
          <tr><td>Stealth</td>         <td class="warning">+4</td>   
                                       <td>+3</td>  
                                       <td>+1</td>    
                                       <td>Jack of All Trades (+1)</td></tr>
          <tr class="active"><td>Constitution</td>    
                                       <td class="active">+2 (14)</td>
                                       <td>13</td>
                                       <td>+1</td>
                                       <td>Human ability increase (+1)</td></tr>
          <tr><td>Con Saving Throw</td><td class="active">+2</td>
                                       <td>+2</td>  
                                       <td>+0</td>
                                       <td></td></tr>
          <tr class="active"><td>Intelligence</td>    
                                       <td class="active">-1 (9)</td>
                                       <td>8</td>
                                       <td>+1</td>
                                       <td>Human ability increase (+1)</td></tr>
          <tr><td>Int Saving Throw</td><td class="active">-1</td>
                                       <td>-1</td>  
                                       <td>+0</td>
                                       <td></td></tr>
          <tr><td>Arcana</td>          <td class="warning">+0</td>   
                                       <td>-1</td>  
                                       <td>+1</td>    
                                       <td>Jack of All Trades (+1)</td></tr>
          <tr><td>History</td>         <td class="warning">+0</td>   
                                       <td>-1</td>  
                                       <td>+1</td>    
                                       <td>Jack of All Trades (+1)</td></tr>
          <tr><td>Investigation</td>   <td class="warning">+0</td>   
                                       <td>-1</td>  
                                       <td>+1</td>    
                                       <td>Jack of All Trades (+1)</td></tr>
          <tr><td>Nature</td>          <td class="warning">+0</td>   
                                       <td>-1</td>  
                                       <td>+1</td>    
                                       <td>Jack of All Trades (+1)</td></tr>
          <tr><td>Religion</td>        <td class="warning">+0</td>   
                                       <td>-1</td>  
                                       <td>+1</td>    
                                       <td>Jack of All Trades (+1)</td></tr>
          <tr class="active"><td>Wisdom</td>    
                                       <td class="active">+0 (10)</td>
                                       <td>9</td>
                                       <td>+1</td>
                                       <td>Human ability increase (+1)</td></tr>
          <tr><td>Wis Saving Throw</td><td class="active">+0</td>
                                       <td>+0</td>  
                                       <td>+0</td>
                                       <td></td></tr>
          <tr><td>Animal Handling</td> <td class="warning">+1</td>   
                                       <td>+0</td>  
                                       <td>+1</td>    
                                       <td>Jack of All Trades (+1)</td></tr>
          <tr><td>Insight</td>         <td class="info">+2</td>   
                                       <td>+0</td>  
                                       <td>+2</td>    
                                       <td>Proficiency (+2)</td></tr>
          <tr><td>Medicine</td>        <td class="warning">+1</td>   
                                       <td>+0</td>  
                                       <td>+1</td>    
                                       <td>Jack of All Trades (+1)</td></tr>
          <tr><td>Perception</td>      <td class="info">+2</td>   
                                       <td>+0</td>  
                                       <td>+2</td>    
                                       <td>Proficiency (+2)</td></tr>
          <tr><td>Survival</td>        <td class="warning">+1</td>   
                                       <td>+0</td>  
                                       <td>+1</td>    
                                       <td>Jack of All Trades (+1)</td></tr>
          <tr class="active"><td>Charisma</td>    
                                       <td class="active">+4 (19)</td>
                                       <td>18</td>
                                       <td>+1</td>
                                       <td>Human ability increase (+1)</td></tr>
          <tr><td>Cha Saving Throw</td><td class="info">+6</td>
                                       <td>+4</td>  
                                       <td>+2</td>
                                       <td>Proficiency (+2)</td></tr>
          <tr><td>Deception</td>       <td class="warning">+5</td>   
                                       <td>+4</td>  
                                       <td>+1</td>    
                                       <td>Jack of All Trades (+1)</td></tr>
          <tr><td>Intimidation</td>    <td class="warning">+5</td>   
                                       <td>+4</td>  
                                       <td>+1</td>    
                                       <td>Jack of All Trades (+1)</td></tr>
          <tr><td>Performance</td>     <td class="info">+6</td>   
                                       <td>+4</td>  
                                       <td>+2</td>    
                                       <td>Proficiency (+2)</td></tr>
          <tr><td>Persuasion</td>      <td class="warning">+5</td>   
                                       <td>+4</td>  
                                       <td>+1</td>    
                                       <td>Jack of All Trades (+1)</td></tr>
      </table>
    </div>
  </body>
</html>
